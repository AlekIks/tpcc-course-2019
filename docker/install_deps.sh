#!/bin/sh

set -e -x

apt-get update

apt-get install -y \
	make \
	cmake \
	ninja-build \
	git \
	clang-6.0 \
	clang-format \
	clang-tidy \
	python3 \
	python3-pip \
	vim

pip3 install \
	click \
	gitpython \
	termcolor \
	virtualenv

