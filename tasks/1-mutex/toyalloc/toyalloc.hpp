#pragma once

#include <twist/memory/mmap_allocation.hpp>

namespace toyalloc {

void Init(twist::MmapAllocation arena);

// Returns arena memory span
twist::MemSpan GetArena();

// Allocates page
void* Allocate();

// Releases previously allocated page
void Free(void* addr);

}  // namespace toyalloc
