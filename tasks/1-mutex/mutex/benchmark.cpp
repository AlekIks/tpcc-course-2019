#include <benchmark/benchmark.h>

#include "mutex.hpp"

#include <mutex>

static void BM_StdMutexNoContention(benchmark::State& state) {
  std::mutex mutex;
  volatile int count = 0;

  for (auto _ : state) {
    std::lock_guard<std::mutex> lock(mutex);
    ++count;
  }
}

BENCHMARK(BM_StdMutexNoContention);

static void BM_SolutionMutexNoContention(benchmark::State& state) {
  tpcc::solutions::Mutex mutex;
  volatile int count = 0;

  for (auto _ : state) {
    mutex.Lock();
    ++count;
    mutex.Unlock();
  }
}

BENCHMARK(BM_SolutionMutexNoContention);

BENCHMARK_MAIN();

