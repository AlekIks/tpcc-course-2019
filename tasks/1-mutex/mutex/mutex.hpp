#pragma once

#include "futex.hpp"

#include <twist/stdlike/atomic.hpp>

#include <mutex>

namespace solutions {

class Mutex {
 public:
  void Lock() {
    mutex_.lock();  // To be implemented
  }

  void Unlock() {
    mutex_.unlock();  // To be implemented
  }

 private:
  std::mutex mutex_;
};

}  // namespace solutions
