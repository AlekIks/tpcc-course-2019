#!/usr/bin/env python3

import tpcc
from tpcc import Config, Tasks, Solutions, ClangCxxCompiler, ClangFormat, ClangTidy
from tpcc import echo
from tpcc.exceptions import ClientError
from tpcc import helpers
from tpcc import highlight
from tpcc import greeting

import argparse
import contextlib
import getpass
import logging
import os
import platform
import shutil
import sys
import subprocess
import traceback

logging.basicConfig(
    format="%(asctime)s\t%(levelname)s\t%(message)s",
    level=logging.INFO,
    datefmt="%Y-%m-%d %H:%M:%S")

try:
    import git
except ImportError:
    logging.fatal(
        "please install 'gitpython' package: https://gitpython.readthedocs.io/en/stable/intro.html")
    sys.exit(1)

try:
    import click
except ImportError:
    logging.fatal(
        "please install 'click' package: https://click.palletsprojects.com")
    sys.exit(1)

# --------------------------------------------------------------------


def check_call(cmd, die_on_error=True, **kwargs):
    logging.debug("Running command {}".format(cmd))

    tool = cmd[0]

    sys.stdout.write('\n{} output:\n'.format(highlight.path(tool)))  # header

    try:
        subprocess.check_call(cmd, **kwargs)
    except subprocess.CalledProcessError as error:
        if (die_on_error):
            echo.error(str(error))
            sys.exit(1)
        else:
            raise

    sys.stdout.write("\n")  # empty footer line


# --------------------------------------------------------------------

def print_hello():
    hello = greeting.pick()
    username = getpass.getuser()
    echo.echo(hello.format(highlight.user(username)))


def print_environment():
    echo.echo("Platform: {}".format(platform.platform()))

    try:
        compiler = ClangCxxCompiler.locate()
    except tpcc.exceptions.ToolNotFound as error:
        echo.error(str(error))
        sys.exit(1)

    echo.echo("C++ compiler: {} ({})".format(
        highlight.path(compiler.binary), compiler.version))

    echo.echo(
        "Python: {}, {}, {}".format(
            platform.python_version(),
            platform.python_implementation(),
            highlight.path(sys.executable)))


def print_local_repo(git_repo):
    echo.echo("Repository root directory: {}".format(
        highlight.path(git_repo.working_tree_dir)))
    echo.echo("Git current commit: {}".format(git_repo.head.commit.hexsha))


def print_headers():
    echo.separator_line()
    print_hello()
    echo.blank_line()
    print_environment()


print_headers()

# --------------------------------------------------------------------

# Build directory ("build" directory in course repo)


class Build(object):
    class Profile(object):
        def __init__(self, name, entries):
            self.name = name
            self.entries = entries

    def __init__(self, git_repo):
        self.path = os.path.join(git_repo.working_tree_dir, 'build')
        self.profiles = self._load_profiles(self.path)

    @staticmethod
    def _load_profiles(build_dir):
        profiles_conf_path = os.path.join(build_dir, 'profiles.json')

        if not os.path.exists(profiles_conf_path):
            raise ClientError(
                "Build profiles not found at".format(
                    highlight.path(profiles_conf_path)))

        try:
            profiles_json = helpers.load_json(profiles_conf_path)
        except BaseException:
            raise ClientError(
                "Cannot load build profiles from {}".format(profiles_conf_path))

        profiles = []
        for name, entries in profiles_json.items():
            profiles.append(Build.Profile(str(name), entries))

        return profiles

    def list_profile_names(self):
        return [p.name for p in self.profiles]

    def _dir(self, profile):
        return os.path.join(self.path, profile.name)

    def _clear_all_dirs(self):
        for subdir in helpers.get_immediate_subdirectories(self.path):
            shutil.rmtree(subdir)

    def _create_profile_dirs(self):
        for profile in self.profiles:
            profile_dir = self._dir(profile)
            helpers.mkdir(profile_dir, parents=True)

    def reset(self):
        self._clear_all_dirs()
        self._create_profile_dirs()

    def profile_build_dirs(self):
        for profile in self.profiles:
            profile_dir = self._dir(profile)
            if not os.path.exists(profile_dir):
                helpers.mkdir(profile_dir, parents=True)
            os.chdir(profile_dir)
            yield profile, profile_dir

    def _find_profile(self, name):
        for profile in self.profiles:
            if profile.name == name:
                return profile
        raise ClientError("Build profile '{}' not found".format(name))

    @contextlib.contextmanager
    def profile(self, name):
        selected_profile = self._find_profile(name)
        cwd = os.getcwd()
        profile_dir = self._dir(selected_profile)
        if not os.path.exists(profile_dir):
            helpers.mkdir(profile_dir, parents=True)
        os.chdir(profile_dir)
        try:
            yield selected_profile
        finally:
            os.chdir(cwd)

    @staticmethod
    def _make_cmake_command(profile):
        def prepend(prefix, items):
            return [prefix + item for item in items]

        cxx_compiler = ClangCxxCompiler.locate()

        common_entries = [
            "CMAKE_CXX_COMPILER={}".format(cxx_compiler.binary),
            "TOOL_BUILD=ON",
            "TWIST_TESTS=ON"
        ]

        entries = profile.entries + common_entries

        return ["cmake"] + prepend("-D", entries) + ["../.."]

    def cmake(self):
        helpers.check_tool("cmake")

        for profile, build_dir in self.profile_build_dirs():
            echo.echo("Generate build scripts for profile {}".format(
                highlight.smth(profile.name)))
            cmake_cmd = self._make_cmake_command(profile)
            check_call(cmake_cmd)


# --------------------------------------------------------------------

def git_repo_root_dir(cwd):
    output = subprocess.check_output(
        ["git", "rev-parse", "--show-toplevel"], cwd=cwd)
    return output.strip().decode("utf-8")


def locate_this_tool_repo():
    this_tool_real_path = os.path.realpath(__file__)
    repo_root_dir = git_repo_root_dir(os.path.dirname(this_tool_real_path))
    return git.Repo(repo_root_dir)


# --------------------------------------------------------------------

git_repo = locate_this_tool_repo()

print_local_repo(git_repo)
echo.blank_line()

build = Build(git_repo)
tasks = Tasks(git_repo)
config = Config(git_repo)

ci_config = os.path.join(git_repo.working_tree_dir, ".gitlab-ci.yml")
solutions = Solutions(config, ci_config)

# --------------------------------------------------------------------


def make_target(target):
    check_call(["make", target])


# --------------------------------------------------------------------

def print_current_dir_task():
    current_task = tasks.current_dir_task()

    if current_task:
        echo.echo("At homework {}, task {}".format(
            highlight.homework(current_task.homework), highlight.task(current_task.name)))
    else:
        echo.echo("Not in task directory: {}".format(
            highlight.path(os.getcwd())))


def current_dir_task_or_die():
    current_dir = os.getcwd()
    current_dir_task = tasks.get_dir_task(current_dir)
    if current_dir_task is None:
        raise ClientError(
            "Not in task directory: {}".format(current_dir))
    return current_dir_task


def switch_to_current_dir_task_or_die():
    current_dir_task = current_dir_task_or_die()
    os.chdir(current_dir_task.dir)
    return current_dir_task

# --------------------------------------------------------------------

# CLI commands


def update_command(args):
    os.chdir(git_repo.working_tree_dir)

    echo.echo("Fetching from and integrating with origin/master\n")
    subprocess.check_call(["git", "pull", "origin", "master"])
    subprocess.check_call(
        ["git", "submodule", "update", "--init", "--recursive"])

    echo.blank_line()
    build.cmake()

    echo.echo("Done.")


def cmake_command(args):
    if args.clean:
        build.reset()
    build.cmake()

    echo.done()


def status_command(args):
    print_current_dir_task()


def test_all_profiles(task):
    blacklist = ["Release"]  # almost all

    test_profiles = task.conf.test_profiles
    if not test_profiles:
        test_profiles = build.list_profile_names()

    echo.echo("Build profiles: {}".format(test_profiles))

    for profile_name in test_profiles:
        if profile_name in blacklist:
            continue
        test_profile(task, profile_name)

    echo.echo("All {}/{} tests completed!".format(
        highlight.homework(task.homework), highlight.task(task.name)))


def test_profile(task, profile_name):
    with build.profile(profile_name):
        echo.echo("Test task {}/{} in profile {}".format(
            highlight.homework(task.homework), highlight.task(task.name), highlight.smth(profile_name)))

        make_target(task.all_tests_target)


def test_command(args):
    current_task = switch_to_current_dir_task_or_die()

    if args.profile:
        test_profile(current_task, args.profile)
    else:
        test_all_profiles(current_task)

    echo.done()


def benchmark_command(args):
    current_task = switch_to_current_dir_task_or_die()

    with build.profile("Release"):
        make_target(current_task.benchmark_target)

    echo.done()


def get_current_dir_lint_targets():
    return helpers.filter_out(
        helpers.glob_expand(["*.hpp", "*.cpp"]),
        blacklist=["unit_test.cpp", "stress_test.cpp", "benchmark.cpp"])


def lint(current_task):
    lint_targets = current_task.conf.lint_files
    if lint_targets is None:
        lint_targets = get_current_dir_lint_targets()

    if not lint_targets:
        echo.echo("Nothing to lint")
        return

    for f in lint_targets:
        if not os.path.exists(f):
            raise tpcc.exceptions.ClientError(
                "Lint target not found: '{}'".format(f))

    # clang-tidy

    clang_tidy = ClangTidy.locate()

    include_dirs = [
        os.path.join(git_repo.working_tree_dir, 'library/twist')
    ]

    echo.echo(
        "Checking {} with clang-tidy ({})".format(lint_targets, clang_tidy.binary))

    if not clang_tidy.check(lint_targets, include_dirs):
        if click.confirm("Do you want to fix these errors?", default=True):
            echo.echo(
                "Applying clang-tidy --fix to {}".format(lint_targets))
            clang_tidy.fix(lint_targets, include_dirs)

    # clang-format

    clang_format = ClangFormat.locate()
    echo.echo(
        "Applying clang-format ({}) to {}".format(clang_format.binary, lint_targets))
    clang_format.apply_to(lint_targets, style="file")


def lint_command(args):
    current_task = switch_to_current_dir_task_or_die()
    lint(current_task)
    echo.done()


def config_command(args):
    config.set_attribute(args.attr, args.value)
    echo.done()


def showconfig_command(args):
    config.show()
    echo.done()


def attach_command(args):
    if not args.url.startswith("https://gitlab.com/") and \
            not args.url.startswith("git@gitlab.com:"):
        raise ClientError(
            "Expected gitlab.com repository, provided: '{}'".format(
                args.url))

    repo_parent_dir = os.path.dirname(git_repo.working_tree_dir)
    os.chdir(repo_parent_dir)

    solutions_repo_dir = os.path.join(repo_parent_dir, args.local_name)

    echo.echo(
        "Clonging solutions repo '{}' to '{}'".format(
            args.url,
            highlight.path(solutions_repo_dir)))

    if os.path.exists(solutions_repo_dir):
        if click.confirm("Do you want remove existing solutions repo '{}'?".format(
                solutions_repo_dir), default=False):
            echo.echo(
                "Remove existing local solutions repo '{}'".format(solutions_repo_dir))
            shutil.rmtree(solutions_repo_dir)
        else:
            # TODO(Lipovsky): interrupted
            sys.exit(1)

    check_call(["git", "clone", args.url, args.local_name],
               cwd=repo_parent_dir)

    echo.echo("Patching client config")
    config.patch({
        "solutions": {
            "url": args.url,
            "path": solutions_repo_dir,
        }
    })

    echo.echo("Solutions repo: {}".format(
        highlight.path(solutions_repo_dir)))

    echo.done()


def commit_command(args):
    current_task = switch_to_current_dir_task_or_die()

    lint(current_task)
    echo.blank_line()
    solutions.commit(current_task, message=args.message)
    echo.done()


def apply_command(args):
    current_task = switch_to_current_dir_task_or_die()
    solutions.apply_to(current_task, args.force)
    echo.done()


def push_command(args):
    current_task = switch_to_current_dir_task_or_die()
    solutions.push(current_task)


def merge_command(args):
    current_task = switch_to_current_dir_task_or_die()
    solutions.merge(current_task)


# --------------------------------------------------------------------

def create_cmdline_parser():
    parser = argparse.ArgumentParser()

    def help_command(args):
        parser.print_help()

    subparsers = parser.add_subparsers()

    help = subparsers.add_parser("help", help="print help")
    help.set_defaults(cmd=help_command)

    update = subparsers.add_parser("update", help="Update local repo")
    update.set_defaults(cmd=update_command)

    cmake = subparsers.add_parser("cmake", help="Generate build scripts")
    cmake.set_defaults(cmd=cmake_command)
    cmake.add_argument(
        "--clean",
        action="store_true",
        help="Remove all existing build scripts in build directory")

    # Task-related commands

    status = subparsers.add_parser(
        "status",
        help="Print current task",
        aliases=["st"])
    status.set_defaults(cmd=status_command)

    test = subparsers.add_parser("test", help="Run tests for current task")
    test.set_defaults(cmd=test_command)
    test.add_argument('-p', "--profile", required=False)

    benchmark = subparsers.add_parser(
        "benchmark", help="Run benchmark for current task", aliases=["bench"])
    benchmark.set_defaults(cmd=benchmark_command)

    lint = subparsers.add_parser(
        "lint",
        help="Apply clang-format and clang-tidy linters to current task sources", aliases=["style"])
    lint.set_defaults(cmd=lint_command)

    config = subparsers.add_parser(
        "config", help="Set client config attributes")
    config.add_argument("attr", help="E.g. path.to.attr")
    config.add_argument("value")
    config.set_defaults(cmd=config_command)

    showconfig = subparsers.add_parser(
        "showconfig", help="Show client config content")
    showconfig.set_defaults(cmd=showconfig_command)

    attach = subparsers.add_parser(
        "attach", help="Attach solutions repo")
    attach.add_argument(
        "url",
        help="E.g. https://gitlab.com/user/solutions.git")
    attach.add_argument(
        "--local-name", help="Local copy name", default="solutions"
    )
    attach.set_defaults(cmd=attach_command)

    commit = subparsers.add_parser(
        "commit", help="Commit current task solution to solutions repo", aliases=["ci"])
    commit.add_argument("-m", "--message", help="Commit message")
    commit.set_defaults(cmd=commit_command)

    apply = subparsers.add_parser(
        "apply", help="Apply solution from solutions repo to current task")
    apply.add_argument("-f", "--force", action="store_true", default=False)
    apply.set_defaults(cmd=apply_command)

    push = subparsers.add_parser(
        "push", help="Push task branch to remote")
    push.set_defaults(cmd=push_command)

    merge = subparsers.add_parser(
        "merge", help="Submit current task solution")
    merge.set_defaults(cmd=merge_command)

    return parser


def main():
    parser = create_cmdline_parser()
    args = parser.parse_args()

    if "cmd" not in args:
        parser.print_help()
        sys.exit(2)

    try:
        args.cmd(args)
    except KeyboardInterrupt:
        echo.error("Exiting on user request\n")
        sys.exit(1)
    except ClientError as error:
        echo.error(str(error))
        sys.exit(1)
    except Exception as e:
        print()
        print(e, file=sys.stderr)
        traceback.print_exc()
        sys.exit(1)

# --------------------------------------------------------------------


if __name__ == "__main__":
    main()
