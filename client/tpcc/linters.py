import subprocess

from .exceptions import ToolNotFound
from . import helpers


class ClangFormat(object):
    def __init__(self, binary):
        self.binary = binary

    @classmethod
    def locate(cls):
        binary = helpers.locate_binary(
            ["clang-format++9.0", "clang-format++8.0", "clang-format++7.0", "clang-format"])
        if not binary:
            raise ToolNotFound(
                "'clang-format' tool not found. See https://clang.llvm.org/docs/ClangFormat.html")
        return cls(binary)

    def apply_to(self, targets, style):
        cmd = [self.binary, "-style", style, "-i"] + targets
        subprocess.check_call(cmd)


class ClangTidy(object):
    def __init__(self, binary):
        self.binary = binary

    @classmethod
    def locate(cls):
        binary = helpers.locate_binary(["clang-tidy"])
        if not binary:
            raise ToolNotFound(
                "'clang-tidy' tool not found. See http://clang.llvm.org/extra/clang-tidy/")
        return cls(binary)

    def _make_command(self, targets, include_dirs, fix=True):
        cmd = [self.binary] + targets + ["--quiet"]
        if fix:
            cmd.append("--fix")

        cmd.append("--")

        # TODO(Lipovsky): customize
        cmd.append("-std=c++17")

        for dir in include_dirs:
            cmd.extend(["-I", str(dir)])

        return cmd

    def check(self, targets, include_dirs):
        cmd = self._make_command(targets, include_dirs, fix=False)
        tidy = subprocess.Popen(cmd)
        exitcode = tidy.wait()
        # todo: separate style errors from all other errors
        return exitcode == 0

    def fix(self, targets, include_dirs):
        cmd = self._make_command(targets, include_dirs, fix=True)
        tidy = subprocess.Popen(cmd)
        tidy.wait()  # intentionally ignore exit code
